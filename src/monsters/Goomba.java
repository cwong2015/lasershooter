package monsters;

import factory.Monster;
import javafx.scene.image.Image;

public class Goomba extends Monster{
	
	public Goomba(){
		super();
		this.health = 10;
		this.attackDamage = 1;
		this.description = "Goomba: A common monster found in Super Mario Brothers.";
		this.image = new Image("resources/Goomba.png");
		
	}
	
}
