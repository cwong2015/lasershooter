package game;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import monsters.Goomba;
import musichandlers.BackgroundMusic;
import musichandlers.SoundFX;

public class Game extends Application {
	private Circle circle;
	private BorderPane fp;
	private int xposition = 300;
	private int yposition = 40;
	private int radius = 10;
	private int laserPositionX, laserPositionY;
	private Image map;
	private ImageView mapView;
	private BackgroundMusic bg;
	private double volume = 1;
	private SoundFX sfx;
	private SoundFX sfx2;
	private KeyValue kv;
	private KeyFrame kf;

	@Override
	public void start(Stage stage) {
		map = new Image("/resources/GameMap.png");
		mapView = new ImageView(map);

		fp = new BorderPane();
		fp.getChildren().addAll(mapView);

		renderBorderPane(fp);

		initializeSounds();
		bg.playSound(true, volume);

		EventHandler<KeyEvent> keyListener = new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if ((event.getCode() == KeyCode.RIGHT || 
					event.getCode() == KeyCode.D) && xposition < fp.getWidth()) {
					xposition += radius / 2;
					printCoor();
					renderBorderPane(fp);
				}
				if ((event.getCode() == KeyCode.DOWN || 
						event.getCode() == KeyCode.S)&& yposition < fp.getHeight()) {
					yposition += radius / 2;
					printCoor();
					renderBorderPane(fp);
				}
				if ((event.getCode() == KeyCode.LEFT || 
						event.getCode() == KeyCode.A)&& xposition > 0) {
					xposition -= radius / 2;
					printCoor();
					renderBorderPane(fp);
				}
				if ((event.getCode() == KeyCode.UP ||
						event.getCode() == KeyCode.W)&& yposition > 0) {
					yposition -= radius / 2;
					printCoor();
					renderBorderPane(fp);
				}
				if (event.getCode() == KeyCode.SPACE) {
					laserPositionX = (int) (circle.getCenterX() + radius);
					laserPositionY = (int) circle.getCenterY();
					shootLaser(fp);

				}
			}

			private void printCoor() {
				System.out.println("COORDINATES: " + "(" + xposition + "," + " " + yposition + ")");
				enemyCollision();
			}

			private void enemyCollision() {
				if (circle.getCenterX() - radius <= (double) kv.getTarget().getValue()
						&& circle.getCenterX() + radius >= (double) kv.getTarget().getValue()
						&& circle.getCenterY() - 3 * radius <= ((fp.getWidth() / 2))
						&& circle.getCenterY() + 3 * radius >= ((fp.getWidth() / 2))) {
					System.out.println("Enemy attacked you");
					playBumpSound();
				}
			}

		};
		Scene scene = new Scene(fp, 540, 360);
		renderEnemy();
		scene.setOnKeyPressed(keyListener);
		stage.setScene(scene);
		stage.setTitle("Laser Shooter");
		stage.setResizable(false);
		stage.sizeToScene();
		stage.show();
	}

	private void initializeSounds() {
		bg = BackgroundMusic.createBackgroundMusic("/resources/WestCity.wav", volume);
		sfx = new SoundFX("/resources/laser_sound_effect.wav", volume);
		sfx2 = new SoundFX("/resources/bump.wav", volume);

	}
	
	private void playBumpSound() {
		sfx2.playSound(false, volume);
		
	}

	private void renderBorderPane(BorderPane fp) {
		fp.getChildren().removeAll(circle);

		circle = new Circle();
		circle.setRadius(radius);
		circle.setCenterX(xposition);
		circle.setCenterY(yposition);
		circle.setFill(Color.LIGHTBLUE);
		circle.setVisible(true);

		fp.getChildren().add(circle);

	}

	private void renderEnemy() {
		Goomba goomba = new Goomba();
		ImageView goombaI = goomba.getImage();
		goombaI.setX(fp.getHeight()/2 + 250);
		goombaI.setY(fp.getWidth()/2);
		fp.getChildren().add(goombaI);

		Timeline timeline = new Timeline();
		kv = new KeyValue(goombaI.xProperty(), fp.getHeight(), Interpolator.EASE_BOTH);
		kf = new KeyFrame(Duration.millis(2000), kv);
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.setAutoReverse(true);
		timeline.getKeyFrames().add(kf);
		timeline.play();
	}

	private void shootLaser(BorderPane fp) {
		Rectangle line = new Rectangle(radius * 5, radius / 3, radius * 5, radius / 3);
		line.setFill(Color.RED);
		line.setX(laserPositionX);
		line.setY(laserPositionY - radius / 2);
		line.setVisible(true);
		playLaserSound();
		fp.getChildren().add(line);

		final Timeline timeline = new Timeline();
		final KeyValue kv = new KeyValue(line.xProperty(), fp.getWidth(), Interpolator.EASE_BOTH);
		final KeyFrame kf = new KeyFrame(Duration.millis(500), kv);
		timeline.getKeyFrames().add(kf);
		timeline.setOnFinished((event) -> {
			fp.getChildren().remove(line);
		});
		timeline.play();

	}

	private void playLaserSound() {
		sfx.playSound(false, volume);
	}

	public static void main(String[] args) {
		launch(args);
	}

}
