package musichandlers;

import factory.Sound;

public class BackgroundMusic extends Sound {
	private static BackgroundMusic singleton = null;

	private BackgroundMusic(String file, double volume) {
		super(file, volume);
	}
	
	public static BackgroundMusic createBackgroundMusic(String file, double volume){
		if (singleton == null){
			singleton = new BackgroundMusic(file, volume);
		}
		return singleton;
	}

}
