package musichandlers;

import factory.Sound;

public class SoundFX extends Sound {

	public SoundFX(String file, double volume) {
		super(file, volume);
	}
}
