package factory;

import java.net.URL;

import javafx.scene.media.AudioClip;

public abstract class Sound {
	protected String file = "";
	protected URL resource = null;
	protected AudioClip clip = null;
	protected double volume = 1.0;

	public Sound(String file, double volume) {
		this.file = file;
		this.volume = volume;
	}

	public void playSound(boolean background, double volume) {
		try {
			resource = getClass().getResource(file);
			clip = new AudioClip(resource.toString());
			if(background){
				clip.setCycleCount(AudioClip.INDEFINITE);
			}
			clip.setVolume(volume);
			clip.play();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
