package factory;

import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class Monster {
	protected int health;
	protected int attackDamage;
	protected Image image;
	protected String description;
	protected ImageView imageView;

	public ImageView getImage() {
		this.imageView = new ImageView(this.image);
		this.imageView.setCursor(Cursor.HAND);
		this.imageView.setOnMouseClicked((e) -> {
			System.out.println(this.description);
		});
		return this.imageView;
	}
}
